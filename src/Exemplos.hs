

{--

Módulo que define exemplos de entrada para o programa.

Cada exemplo consiste em uma tupla com os parâmetros de entrada e uma tupla com as
saídas esperadas de cada função, no formato:

  exemplo = (n, cpal, pal, lim)

  saida = (ocorremNoQuadro, nocorrencias, palavrasmaiscaras, infoPalavraEncontradas, direcaoMaisComum)

--}


module Exemplos (exemplos) where

type Exemplo = ( Int, String, [(Char,Int)], [String], Int )
type Saida = ( [(String,Int,Bool)], [(String,Int)], [(String,Int)], [(String, [(Int,String)])], [String] )

exemplos :: [ (Exemplo,Saida) ]
exemplos = [(exemplo0,saida0), (exemplo1,saida1), (exemplo2,saida2), (exemplo3,saida3)]


-- EXEMPLO 0

exemplo0 = (
    10, -- tamanho
    concat [  -- matriz
        "FLORESTASR",
        "KJGDFTJJUV",
        "CAPELAPISC",
        "DTEEDWYERT",
        "FDSCSALUSE",
        "QLOCAOEAIO",
        "AASARPRDKA",
        "NENEFTEBAU",
        "JKDDSHWLFL",
        "IOTUGJJVAA" ],
    [   -- custos
      ('A',1),('B',10),('C',18),('D',38),('E',70),('F',10),('G',43),('H',15),('I',54),
      ('J',10),('K',44),('L',81),('M',19),('N',39),('O',54),('P',91),('Q',72),('R',30),
      ('S',84),('T',75),('U',22),('V',47),('W',66),('X',66),('Y',59),('Z',32)
    ],
    ["AULA", "ALEGRIA", "CAPELA", "FLORESTA", "LAPIS", "PESO"], -- palavras
    200   -- limite dos custos
  )

saida0 = (
    [("AULA",105,True), ("ALEGRIA",280,False), ("CAPELA",262,True), ("FLORESTA",405,True), ("LAPIS",311,True), ("PESO",299,True)], -- ocorremNoQuadro
    [("AULA",1), ("CAPELA",2), ("FLORESTA",1), ("LAPIS",1), ("PESO",3)], --nocorrencias
    [("LAPIS",311),("FLORESTA",405),("CAPELA",524),("PESO",897)], -- palavrasmaiscaras
    [ -- infoPalavraEncontradas
      ("AULA",    [(69,"vertical-topo-base")]),
      ("CAPELA",  [(20,"horizontal-esquerda-direita"), (43,"diagonal-topo-base")]),
      ("FLORESTA",[(0,"horizontal-esquerda-direita")]),
      ("LAPIS",   [(24,"horizontal-esquerda-direita")]),
      ("PESO",    [(22,"vertical-topo-base"), (22,"diagonal-topo-base"),(26,"diagonal-topo-base")])
    ],
    ["horizontal", "diagonal"]  -- direcaoMaisComum
  )



-- EXEMPLO 1

exemplo1 = (
    10,
    concat [
      "DBMNYCIBNR",
      "AEKOWOKAYY",
      "NDUZDBSHXX",
      "IXAQTAQBOP",
      "LRCLXLVUEA",
      "OGZDTTRDEW",
      "PIBFMODATN",
      "BLITZKRIEG",
      "AXDFDDRJWQ",
      "DGXNVLKXZB"
      ],
    [
      ('A',2),('B',4),('C',5),('D',7),('E',9),('F',5),('G',9),('H',3),('I',3),
      ('J',27),('K',25),('L',7),('M',4),('N',8),('O',3),('P',7),('Q',97),('R',11),
      ('S',3),('T',1),('U',12),('V',27),('W',3),('X',3334),('Y',11),('Z',334)
    ],
    [ "DANILO", "MODA", "COBALTO", "ADALTO", {-"ASA",-} "BUDA", "BLITZKRIEG", "FANTASMA" ],
    30
  )


saida1 = (
    [("DANILO",30,True), ("MODA",16,True), ("COBALTO",25,True), ("ADALTO",22,True), {-("ASA",7,True),-} ("BUDA",25,True), ("BLITZKRIEG",406,True), ("FANTASMA",27,False)], -- ocorremNoQuadro
    [("DANILO",1),("MODA",2),("COBALTO",1),("ADALTO",1),{-("ASA",1),-}("BUDA",1),("BLITZKRIEG",1) ], --nocorrencias
    [("DANILO",30), ("MODA",32), ("BLITZKRIEG",406)], -- palavrasmaiscaras
    [ -- infoPalavraEncontradas
      ("DANILO", [(00, "vertical-topo-base")]),
      ("MODA", [(02, "diagonal-topo-base"),(64, "horizontal-esquerda-direita")]),
      ("COBALTO", [(05, "vertical-topo-base")]),
      ("ADALTO", [(10, "diagonal-topo-base")]),
      --("ASA", [(35, "diagonal-base-topo")]),
      ("BUDA", [(37, "vertical-topo-base")]),
      ("BLITZKRIEG", [(70, "horizontal-esquerda-direita")])
    ],
    ["vertical"]  -- direcaoMaisComum
  )


-- EXEMPLO 2

exemplo2 = (
    11,
    concat ["AAFHATAGEAT",
            "TARAHRUBROA",
            "EAFCNASTERE",
            "KRTKAPBUABY",
            "KTBERNATBUS",
            "ADVRANOTTRE",
            "IIUMBAHTRSD",
            "QYEAICSRAID",
            "DUINVUVBKAA",
            "QUELICERADK",
            "AVPATOLOGIA"
            ],
          [
            ('A',1),('B',2),('C',3),('D',4),('E',5),('F',6),('G',7),('H',8),('I',9),('J',10),('K',11),('L',12),('M',13),('N', 14),
            ('O',15),('P',16),('Q',17),('R',18),('S',19),('T',20),('U',21),('V',22),('W',23),('X',24),('Y',25),('Z',26)
          ],
          [ "TEKKA", "PATOLOGIA", "RUBRO", "ARANHA", "QUELICERA", "LEI", "PATRIA", "ANARQUIA", "HACKERMAN" ],
          48
        )
saida2 = (
    [("TEKKA",48,True), ("PATOLOGIA",96,True), ("RUBRO",74,True), ("ARANHA",43,True), ("QUELICERA",91,True), ("LEI",26,False), ("PATRIA",65,True), ("ANARQUIA",82,False), ("HACKERMAN",74,True)], -- ocorremNoQuadro
    [("TEKKA",1),("PATOLOGIA",1),("RUBRO",2),("ARANHA",1),("QUELICERA",1),("PATRIA",1),("HACKERMAN",1)], -- nocorrencias
    [("TEKKA",48), ("PATRIA",65), ("HACKERMAN",74), ("QUELICERA",91), ("PATOLOGIA",96), ("RUBRO",148)], -- palavrasmaiscaras
    [ -- infoPalavraEncontradas
      ("TEKKA",[(11,"vertical-topo-base")]),
      ("PATOLOGIA",[(112,"horizontal-esquerda-direita")]),
      ("RUBRO",[(16,"horizontal-esquerda-direita"),(64,"vertical-base-topo")]),
      ("ARANHA",[(59,"vertical-base-topo")]),
      ("QUELICERA",[(99,"horizontal-esquerda-direita")]),
      ("PATRIA",[(38,"diagonal-topo-base")]),
      ("HACKERMAN",[(3,"vertical-topo-base")])
    ],
    ["vertical"] -- direcaoMaisComum

  )


-- EXEMPLO 3

exemplo3 = (
    20,
    concat [
      "QZDFIXUZHCEIUBNNQFNG",
      "MPKPSWNHUECPPACIAJDR",
      "ZNMUOOWNIRAHDUMRCDTK",
      "GOWYVBALOTRACDROCNUF",
      "VORRDOUJLALAUDEXRFRM",
      "IPXRTPDIQMCEQAHOERKN",
      "AYGVAQGWJEGERRXYTSAB",
      "EAHBEMQSZNVDURLECDFM",
      "BWSTHCJBLTGXDWANRPIB",
      "CADSEMPREENDEDORISMO",
      "JJPIAFCULVBSCBUZKNWR",
      "OKEMFDTYVTGFFWTBHZQX",
      "TEGZTKOMLATALBKLCKQJ",
      "EJSRPJAKTBKKDRJQFKVL",
      "KUPTROSKCIUQQVIFIDJR",
      "KCIJXVRXFAZAUUQNQLGO",
      "AGXBIRUDEDPTDEISEJYW",
      "HGHLCUCUZJVGTXBATMAN",
      "EPRYDCEMHVGLHPZEWWJN",
      "BZXOKKRHLFUSPPORCOFA"
    ],[
      ('A',39),('B',56),('C',55),('D',25),('E',71),('F',29),('G',86),('H',95),('I',30),
      ('J',13),('K',98),('L',27),('M',34),('N',42),('O',75),('P',92),('Q',60),('R',14),
      ('S',47),('T',3),('U',41),('V',59),('W',20),('X',7),('Y',72),('Z',68)
    ],[
      "CERTAMENTE", "HITLER", "BATMAN", "RUDE", "ALAUDE", "ERRAR", "QUICKSORT",
      "QUEBEC", "CARTOLA", "RECURSAO", "ARROZ", "MARROM", "NEIN", "SEMPRE",
      "EMPREENDEDORISMO", "LATA", "NOPE", "PORCO", "ASSADO", "ROMA", "TEKKA"
    ],
    300
  )

saida3 = (
    [ -- ocorremNoQuadro
      ("CERTAMENTE",403,True), ("HITLER",240,True), ("BATMAN",213,True), ("RUDE",151,True),
      ("ALAUDE",242,True), ("ERRAR",152,True), ("QUICKSORT",423,True), ("QUEBEC",354,True),
      ("CARTOLA",252,True), ("RECURSAO",356,True), ("ARROZ",210,True), ("MARROM",210,True),
      ("NEIN",185,False), ("SEMPRE",329,True), ("EMPREENDEDORISMO",791,True), ("LATA",108,True),
      ("NOPE",280,False), ("PORCO",311,True), ("ASSADO",272,True), ("ROMA",162,True), ("TEKKA",309,True)
    ], [ -- nocorrencias
      ("CERTAMENTE",1),("HITLER",1),("BATMAN",1),("RUDE",2),("ALAUDE",1),("ERRAR",1),
      ("QUICKSORT",1),("QUEBEC",1),("CARTOLA",1),("RECURSAO",1),("ARROZ",1),("MARROM",1),
      ("SEMPRE",1),("EMPREENDEDORISMO",1),("LATA",2),("PORCO",1),("ASSADO",1),("ROMA",1),("TEKKA",1)
    ], [ -- palavrasmaiscaras
      ("RUDE",302), ("TEKKA",309), ("PORCO",311), ("SEMPRE",329), ("QUEBEC",354),
      ("RECURSAO",356), ("CERTAMENTE",403), ("QUICKSORT",423), ("EMPREENDEDORISMO",791)
    ], [ -- infoPalavraEncontradas
      ("CERTAMENTE",[(9, "vertical-topo-base")]),
      ("HITLER",    [(27, "diagonal-topo-base")]),
      ("BATMAN",    [(354, "horizontal-esquerda-direita")]),
      ("RUDE",      [(132, "vertical-topo-base"),(325, "horizontal-esquerda-direita")]),
      ("ALAUDE",    [(89, "horizontal-esquerda-direita")]),
      ("ERRAR",     [(111, "diagonal-topo-base")]),
      ("QUICKSORT", [(291, "horizontal-direita-esquerda")]),
      ("QUEBEC",    [(291, "diagonal-topo-base")]),
      ("CARTOLA",   [(72, "horizontal-direita-esquerda")]),
      ("RECURSAO",  [(386, "vertical-base-topo")]),
      ("ARROZ",     [(124, "diagonal-base-topo")]),
      ("MARROM",    [(159, "diagonal-base-topo")]),
      ("SEMPRE",    [(183, "horizontal-esquerda-direita")]),
      ("EMPREENDEDORISMO", [(184, "horizontal-esquerda-direita")]),
      ("LATA",      [(248, "horizontal-esquerda-direita"),(252, "horizontal-direita-esquerda")]),
      ("PORCO",     [(393, "horizontal-esquerda-direita")]),
      ("ASSADO",    [(141, "diagonal-topo-base")]),
      ("ROMA",      [(96, "diagonal-base-topo")]),
      ("TEKKA",     [(240, "vertical-topo-base")])
    ], [-- direcaoMaisComum
      "horizontal"
    ]
  )
