

{--  Funções para manipular matrizes  --}


module Matriz (
  Mat,
  matQuad,
  matPos,
  matTam,
  todasCoords,
  checaCoord,
) where

import Vetor (Vet)
import Util (idxs, noInterv)

type Mat a = ([a], Int, Int)


-- cria uma tupla que representa uma matriz quadrada a partir do tamanho `n` e
-- uma lista de elementos `xs`, cujo tamanho é verificado
matQuad :: [a] -> Int -> Mat a
matQuad xs n
  | length xs == n^2  = (xs,n,n)
  | otherwise         = error "lista de tamanho incorreto"


-- acessa o elemento da matriz da linha `i` e coluna `j`
matPos :: Mat a -> Vet -> a
matPos (xs,n,m) (i,j)
    | i < 0       = error "índice de linha negativo"
    | i >= n      = error "índice de linha muito grande"
    | j < 0       = error "índice de coluna negativo"
    | j >= m      = error "índice de coluna muito grande"
    | otherwise   = xs !! (i*m+j)


-- retorna uma tupla com o tamanho da matriz
matTam :: Mat a -> Vet
matTam (_,n,m) = (n,m)

-- retorna uma lista com todas coordenadas da matriz
todasCoords :: Mat a -> [Vet]
todasCoords (_,n,m) = [ (i,j) | i <- idxs n, j <- idxs m ]

-- verifica se uma coordenada está dentro dos limites da matriz
checaCoord :: Vet -> Vet -> Bool
checaCoord (n,m) (i,j) = noInterv 0 (n-1) i && noInterv 0 (m-1) j


--
