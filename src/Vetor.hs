
module Vetor (
  Vet,
  zero,
  ehZero,
  checaNaoZero,
  somaVet,
  subVet
) where

type Vet = (Int, Int)


-- vetor nulo
zero :: Vet
zero = (0,0)

-- retorna True se o vetor for nulo
ehZero :: Vet -> Bool
ehZero = (== zero)

-- emite erro se o vetor for nulo
checaNaoZero :: Vet -> Vet
checaNaoZero v = if v == zero then error "o vetor é nulo" else v


somaVet, subVet :: Vet -> Vet -> Vet

-- soma dois vetores
somaVet (a,b) (x,y) = (a+x,b+y)

-- subtrai um vetor pelo outro
subVet (a,b) (x,y) = (a-x,b-y)

--
