

{--  Funções Úteis  --}


module Util (
  idxs,
  noInterv,
  contaBool,
  contaCom,
  conta,
  qSortOn,
) where


-- gera uma lista de índices de uma lista de tamanho 'n'
idxs :: Int -> [Int]
idxs n = [ 0 .. n-1 ]

-- checa se um inteiro está no intervalo a–b
noInterv :: Int -> Int -> Int -> Bool
noInterv a b x = a <= x && x <= b


----

-- funçao if com ordem de parâmetros invertido
-- retorna 'a' se 'c' é verdadeiro, senão, retorna 'b'
escolhe :: a -> a -> Bool -> a
escolhe a b c = if c then a else b

-- conta quantos são verdadeiros em uma lista de booleanos
contaBool :: Integral c => [Bool] -> c
contaBool xs = foldr (escolhe (1+) (0+)) 0 xs

-- conta quantos elementos de um a lista satisfazem ao predicado 'fn'
contaCom :: Integral c => (a -> Bool) -> [a] -> c     -- TODO testar eficiencia
contaCom fn = contaBool . map fn

-- conta quantos elementos de um a lista são iguais ao parametro 'a'
conta :: (Eq a, Integral c) => a -> [a] -> c
conta a = contaCom (==a)

--

-- ordena uma lista usando quicksort de acordo com o valor retornado pela função
-- `fn` para cada elemento da lista
qSortOn :: Ord b => (a -> b) -> [a] -> [a]
qSortOn fn [] = []
qSortOn fn (pivo:xs) = st menores ++ pivo : st maiores
  where
    st = qSortOn fn
    comp = (fn pivo)
    maiores = filter ((>= comp) . fn) xs
    menores = filter ((< comp) . fn) xs


--
