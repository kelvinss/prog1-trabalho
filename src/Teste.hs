

{--  Módulo para teste  --}


import CPal (
  --ocorreNoQuadro,
  ocorremNoQuadro,
  nocorrencias,
  palavrasmaiscaras,
  infoPalavraEncontradas,
  direcaoMaisComum
  )

import Exemplos (exemplos)


-- Constantes para teste no modo interativo

-- seleciona o exemplo 1
(exemplo,saida) = exemplos !! 1

-- valores de entrada
(n,cpalavras,lcustos,palavras,lim) = exemplo
-- valores esperados de saída
(ocorrem, nocor, maiscaras, info, direcao) = saida





-- -- Teste Automático -- --

verbose = True
parar = True

checkEq nome ex res = do
    if verbose
      then do
        print ex
        print res
      else return ()
    if ex == res
      then print ("OK: " ++ nome)
      else
        (if parar then error else print) ("ERROR: " ++ nome)
    putStr "\n"


testa (exemplo,saida)
    = do

      let ocorrem_ = ocorremNoQuadro palavras cpal n lcustos
      checkEq "ocorremNoQuadro" ocorrem ocorrem_

      let nocor_ = nocorrencias palavras cpal n
      checkEq "nocorrencias" nocor nocor_

      let maiscaras_ = palavrasmaiscaras palavras cpal n lcustos lim
      checkEq "palavrasmaiscaras" maiscaras maiscaras_

      let info_ = infoPalavraEncontradas palavras cpal n
      checkEq "infoPalavraEncontradas" info info_

      let direcao_ = direcaoMaisComum palavras cpal n
      checkEq "direcaoMaisComum" direcao direcao_

  where
    (n,cpal,lcustos, palavras, lim) = exemplo
    (ocorrem, nocor, maiscaras, info, direcao) = saida


main = mapM_ fn $ zip [0..] exemplos
  where
    fn (i,exemplo) = do
      putStr $ "\n\n" ++ "EXEMPLO: " ++ show i ++ "\n\n"
      testa exemplo



--
