
-- TRABALHO COMPUTACIONAL – 2017/1 — CCOMP — PROG1

-- Integrantes:

-- Kelvin Steiner Santos
-- Leonardo Fontes Moreira


{--  Organização do Trabalho

Módulos:
  • Matriz.hs : manipula matrizes
      → acesso de posição e obtenção da lista de coordenadas
  • Vetor.hs : manipula vetores bidimensionais, representados por tupla de Int
      → soma de vetores, verifica se é nulo
  • Util.hs : funções úteis
      → contagem de booleanos, contagem com predicado, quicksort, etc
  • Exemplos.hs : módulo com os exemplos de entrada


O script principal contém, na ordem:
  • constantes com valores de exemplo de entrada para teste no modo interativo;

  • listas que armazenam as direções e seus nomes;

  • diversas funções auxiliares que são utilizadas na função fundamental `listaEstaEm`;

  • `listaEstaEm`, utilizada na maioria das funções principais, tem a função
    de determinar se uma palavra `xs` começa em uma posição `pos` da matriz do
    caça-palavras e estende-se em uma direção `dir`, sendo `pos` e `dir` vetores
    bidimensionais de Int;

  • `matUtil`, responsável por receber a matriz e tamanho do caça-palavras e
    retornar uma tupla com todos os objetos utilizados nas funções principais:
    `(mat, coords, palavraEm)`, sendo
          mat     → tupla que representa a matriz, utilizada para acessar as posições
          coords  → lista com todas as coordenadas possíveis da matriz
          palavraEm → função `listaEstaEm` aplicada à matriz dada

  • `pegaCusto` e `calculaCusto`, responsáveis por obter, respectivamente,
    o custo de uma letra ou palavra dada

  • as funcões principais

--}



module CPal (
  ocorreNoQuadro,
  ocorremNoQuadro,
  nocorrencias,
  palavrasmaiscaras,
  infoPalavraEncontradas,
  direcaoMaisComum,
) where

import Data.List (isPrefixOf)

import qualified Matriz as Mt
import qualified Vetor as Vt
import Vetor (Vet)
import Util (contaBool, qSortOn)

import Exemplos (exemplos)


-- Constantes para teste no modo interativo --

-- seleciona o exemplo 1
(exemplo,_) = exemplos !! 1

-- expõe os valores de entrada
(n,cpalavras,lcustos,palavras,lim) = exemplo



-- ===== Funções Auxiliares =====

-- lista que associa o nome de uma direção a um vetor que a representa
direcoes :: [(Vet, String)]
direcoes = [
    (( 0, 1), "horizontal-esquerda-direita"),
    (( 0,-1), "horizontal-direita-esquerda"),
    (( 1, 0), "vertical-topo-base"),
    ((-1, 0), "vertical-base-topo"),
    (( 1, 1), "diagonal-topo-base"),
    ((-1,-1), "diagonal-base-topo")
  ]

-- lista com os "grupos" de direções (paralelas)
gruposDirecoes :: [(String, [Vet])]
gruposDirecoes = [
    ("horizontal",  [ ( 0, 1), ( 0,-1) ] ),
    ("vertical",    [ ( 1, 0), (-1, 0) ] ),
    ("diagonal",    [ ( 1, 1), (-1,-1) ] )
  ]


----

-- calcula a distancia de uma posição até a borda da matriz em determinada direção
distAteBorda :: Vet -> Vet -> Vet -> Int
distAteBorda (t1,t2) dir (p1,p2) =
      minimum [ distLim t d p | (t,d,p) <- [(t1,d1,p1),(t2,d2,p2)], d /= 0 ]
  where
    (d1,d2) = Vt.checaNaoZero dir   -- verifica se o vetor direção não é nulo e decompoe
    distLim t d p =
      if d > 0  -- se o sentido for positivo
        then (t-1) - p    -- a distância até o extremo "superior"
        else p            -- a distância até o extremo "inferior"


-- gera as coordenadas de uma matriz a partir de uma posição em uma determinada direção até a borda
geraCoordsDir :: Vet -> Vet -> Vet -> [Vet]
geraCoordsDir tam _dir pos = take (dist+1) (iterate (Vt.somaVet dir) pos)
  where
    dir = Vt.checaNaoZero _dir        -- checa se a direção é não-nula
    dist = distAteBorda tam dir pos   -- distância até a borda


-- gera uma sublista de uma matriz a partir de uma posição em uma determinada direção
geraSublista :: Mt.Mat a -> Vet -> Vet -> [a]
geraSublista mat dir pos = map (Mt.matPos mat) coords
  where coords = geraCoordsDir (Mt.matTam mat) dir pos


-- verifica se uma palavra 'xs' está em uma posição 'pos' em uma direção 'dir' da matriz 'mat'
listaEstaEm :: Eq a => Mt.Mat a -> [a] -> Vet -> Vet -> Bool
listaEstaEm mat xs pos dir = xs `isPrefixOf` geraSublista mat dir pos


-- apartir de uma lista de elementos e tamanho da matriz quadrada
-- retorna o "objeto" matriz, coordenadas e função de busca
matUtil cpal n = let mat = Mt.matQuad cpal n in
    (mat, Mt.todasCoords mat, listaEstaEm mat)


----

type Custo = (Char, Int)

-- a função 'pegaCusto' procura uma determinada letra como primeiro elemento de
-- uma tupla numa lista que associa as letras com seus custos, e retorna seu custo
-- emite um erro caso não encontre a letra
pegaCusto :: [Custo] -> Char ->  Int
pegaCusto []         k  = error ("o caractere " ++ show k ++ " não está na lista de custos")
pegaCusto ((a,v):xs) k  = if a == k then v else pegaCusto xs k

-- calcula o custo total de uma palavra 'p' a partir de uma lista de custos 'cl'
calculaCusto :: [Custo] -> String -> Int
calculaCusto cl p = sum $ map (pegaCusto cl) p  -- [ pegaCusto cl c | c <- p ]




-- ===== Funções Principais =====

ocorreNoQuadro          :: String -> String -> Int -> Bool
ocorremNoQuadro         :: [String] -> String -> Int -> [Custo]  ->  [(String, Int, Bool)]
nocorrencias            :: [String] -> String -> Int -> [ (String, Int) ]
palavrasmaiscaras       :: [String] -> String -> Int -> [Custo] -> Int -> [(String, Int)]
infoPalavraEncontradas  :: [String] -> String -> Int -> [(String, [(Int, String)])]
ocorrenciasDirecoes     :: [String] -> String -> Int -> [(String, Int)]
direcaoMaisComum        :: [String] -> String -> Int -> [String]



ocorreNoQuadro p cpal n =
    -- verifica se há pelo menos uma ocorrência da palavra, para todas direções e posicões
    or [ palavraEm p coor dir | dir <- dirs, coor <- coords  ]
  where
    (_, coords, palavraEm) = matUtil cpal n
    dirs = map fst direcoes -- extrai os vetores das direções



ocorremNoQuadro pal cpal n cl = [ (p, calculaCusto cl p, ocorreNoQuadro p cpal n ) | p <- pal]



nocorrencias pal cpal n = [ (p,ocors) | p <- pal, let ocors = contaOcorrencias p, ocors > 0 ]
  where
    (_, coords, palavraEm) = matUtil cpal n
    dirs = map fst direcoes
    -- conta a quantidade de ocorrencias de uma palavra `p`, em todas as direções e posições
    contaOcorrencias p = contaBool [ palavraEm p coor dir | dir <- dirs, coor <- coords]



palavrasmaiscaras pal cpal n cl lim =
    -- ordena usando o segundo elemento das tuplas, que é o custo
    qSortOn snd [ (p,custo) |
                      (p,no) <- nocorrencias pal cpal n,    -- obtém o número de ocorrencias de cada palavra
                      let custo = (calculaCusto cl p)*no,   -- calcula o custo da palavra e multiplica pelo nº de ocorrências
                      custo >= lim                          -- filtra os que tem custo maior ou igual que o limite especificado
                ]



infoPalavraEncontradas pal cpal n = [ (p, info) | p <- pal, let info = infoPalavra p, not (null info) ]
  where
    (_, coords, palavraEm) = matUtil cpal n
    infoPalavra p = [ (n*i+j, nomeDir) | pos@(i,j) <- coords, (dir,nomeDir) <- direcoes, palavraEm p pos dir ]



direcaoMaisComum pal cpal n =
    if maiorOcor == 0   -- se não há nenhuma ocorrência de palavra
      -- retorna uma lista vazia
      then []
      -- se não, retorna todas direções com quantidade de ocorrências igual à maior
      else
        [ nome | (nome,n) <- direcs, n == maiorOcor ]
  where
    direcs = ocorrenciasDirecoes pal cpal n
    (_,ocors) = unzip direcs    -- lista apenas com os números de ocorrencias em cada direção
    maiorOcor = maximum ocors   -- o número de ocorrencias maior entre as direções


-- retorna uma lista de tuplas associando o nome de cada "classe" de direções com
-- sua respectiva soma de ocorrências de palavras
ocorrenciasDirecoes pal cpal n = [ (nome, contaDirs dirs) | (nome, dirs) <- gruposDirecoes ]
  where
    (_, coords, palavraEm) = matUtil cpal n
    -- retorna a quantidade de ocorrencias de palavras em todas as direções em uma lista 'dirs'
    contaDirs dirs = contaBool [ palavraEm p coor dir | p <- pal , coor <- coords, dir <- dirs ]



----
